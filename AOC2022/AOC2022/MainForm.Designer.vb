﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.gotoDay1 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button18)
        Me.GroupBox1.Controls.Add(Me.Button19)
        Me.GroupBox1.Controls.Add(Me.Button20)
        Me.GroupBox1.Controls.Add(Me.Button21)
        Me.GroupBox1.Controls.Add(Me.Button22)
        Me.GroupBox1.Controls.Add(Me.Button23)
        Me.GroupBox1.Controls.Add(Me.Button24)
        Me.GroupBox1.Controls.Add(Me.Button25)
        Me.GroupBox1.Controls.Add(Me.Button26)
        Me.GroupBox1.Controls.Add(Me.Button27)
        Me.GroupBox1.Controls.Add(Me.Button28)
        Me.GroupBox1.Controls.Add(Me.Button11)
        Me.GroupBox1.Controls.Add(Me.Button12)
        Me.GroupBox1.Controls.Add(Me.Button13)
        Me.GroupBox1.Controls.Add(Me.Button14)
        Me.GroupBox1.Controls.Add(Me.Button15)
        Me.GroupBox1.Controls.Add(Me.Button16)
        Me.GroupBox1.Controls.Add(Me.Button17)
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.gotoDay1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1488, 765)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Wybierz dzień"
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(615, 280)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(127, 65)
        Me.Button18.TabIndex = 26
        Me.Button18.Text = "Dzień 25"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(482, 280)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(127, 65)
        Me.Button19.TabIndex = 25
        Me.Button19.Text = "Dzień 24"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(349, 280)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(127, 65)
        Me.Button20.TabIndex = 24
        Me.Button20.Text = "Dzień 23"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(216, 280)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(127, 65)
        Me.Button21.TabIndex = 23
        Me.Button21.Text = "Dzień 22"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(804, 199)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(127, 65)
        Me.Button22.TabIndex = 22
        Me.Button22.Text = "Dzień 21"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(671, 199)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(127, 65)
        Me.Button23.TabIndex = 21
        Me.Button23.Text = "Dzień 20"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(538, 199)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(127, 65)
        Me.Button24.TabIndex = 20
        Me.Button24.Text = "Dzień 19"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(405, 199)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(127, 65)
        Me.Button25.TabIndex = 19
        Me.Button25.Text = "Dzień 18"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Location = New System.Drawing.Point(272, 199)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(127, 65)
        Me.Button26.TabIndex = 18
        Me.Button26.Text = "Dzień 17"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.Location = New System.Drawing.Point(139, 199)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(127, 65)
        Me.Button27.TabIndex = 17
        Me.Button27.Text = "Dzień 16"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(6, 199)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(127, 65)
        Me.Button28.TabIndex = 16
        Me.Button28.Text = "Dzień 15"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(804, 111)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(127, 65)
        Me.Button11.TabIndex = 15
        Me.Button11.Text = "Dzień 14"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(671, 111)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(127, 65)
        Me.Button12.TabIndex = 14
        Me.Button12.Text = "Dzień 13"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(538, 111)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(127, 65)
        Me.Button13.TabIndex = 13
        Me.Button13.Text = "Dzień 12"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(405, 111)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(127, 65)
        Me.Button14.TabIndex = 12
        Me.Button14.Text = "Dzień 11"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(272, 111)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(127, 65)
        Me.Button15.TabIndex = 11
        Me.Button15.Text = "Dzień 10"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(139, 111)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(127, 65)
        Me.Button16.TabIndex = 10
        Me.Button16.Text = "Dzień 9"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(6, 111)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(127, 65)
        Me.Button17.TabIndex = 9
        Me.Button17.Text = "Dzień 8"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(804, 19)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(127, 65)
        Me.Button7.TabIndex = 6
        Me.Button7.Text = "Dzień 7"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(671, 19)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(127, 65)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "Dzień 6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(538, 19)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(127, 65)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "Dzień 5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(405, 19)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(127, 65)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Dzień 4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(272, 19)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(127, 65)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Dzień 3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(139, 19)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(127, 65)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Dzień 2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'gotoDay1
        '
        Me.gotoDay1.Location = New System.Drawing.Point(6, 19)
        Me.gotoDay1.Name = "gotoDay1"
        Me.gotoDay1.Size = New System.Drawing.Size(127, 65)
        Me.gotoDay1.TabIndex = 0
        Me.gotoDay1.Text = "Dzień 1"
        Me.gotoDay1.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(963, 399)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "MainForm"
        Me.Text = "Advent Of Code 2022"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents gotoDay1 As System.Windows.Forms.Button

End Class
