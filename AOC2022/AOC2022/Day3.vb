﻿Public Class Day3

    'Declare variables
    Dim functions As New Functions
    Dim data As New List(Of String)
    Dim firstCompartment As String
    Dim secondCompartment As String
    Dim commonElements As New List(Of String)
    Dim commonGroupElements As New List(Of String)
    Dim firstElements As Char()
    Dim firstGroupElements As Char()
    Dim alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    Dim solutionA As Integer = 0
    Dim solutionB As Integer = 0

    Private Sub Day3_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        functions.LoadData(Me.Name, data)

        'PART A'
        For Each item As String In data
            firstCompartment = item.Substring(0, Val(item.Length / 2))
            secondCompartment = item.Substring(Val(item.Length / 2), Val(item.Length / 2))

            firstElements = firstCompartment.ToCharArray()

            For Each element As String In firstElements
                If (secondCompartment.IndexOf(element) >= 0) Then
                    commonElements.Add(element)
                    Exit For
                End If
            Next
        Next

        For Each element As String In commonElements
            solutionA += Val(alphabet.IndexOf(element)) + 1
        Next

        'PART B'
        Dim itemsGroups = functions.CreateSublistsFromListOfString(Val(data.Count / 3), data)
        For Each itemGroup As List(Of String) In itemsGroups
            firstGroupElements = itemGroup(0).ToCharArray()

            For Each element As String In firstGroupElements
                If (itemGroup(1).IndexOf(element) >= 0) Then
                    If (itemGroup(2).IndexOf(element) >= 0) Then
                        commonGroupElements.Add(element)
                        Exit For
                    End If
                End If
            Next
        Next

        For Each groupElement As String In commonGroupElements
            solutionB += Val(alphabet.IndexOf(groupElement)) + 1
        Next

        answerA.Text = solutionA
        answerB.Text = solutionB
    End Sub

    Private Sub goBack_Click(sender As System.Object, e As System.EventArgs) Handles goBack.Click
        Me.Hide()
        MainForm.Show()
    End Sub
End Class