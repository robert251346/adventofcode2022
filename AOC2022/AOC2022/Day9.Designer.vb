﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Day9
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.goBack = New System.Windows.Forms.Button()
        Me.answerA = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.answerB = New System.Windows.Forms.TextBox()
        Me.SuspendLayout
        '
        'goBack
        '
        Me.goBack.Location = New System.Drawing.Point(301, 124)
        Me.goBack.Name = "goBack"
        Me.goBack.Size = New System.Drawing.Size(147, 48)
        Me.goBack.TabIndex = 0
        Me.goBack.Text = "Powrót do ekranu głównego"
        Me.goBack.UseVisualStyleBackColor = true
        '
        'answerA
        '
        Me.answerA.Location = New System.Drawing.Point(63, 61)
        Me.answerA.Multiline = true
        Me.answerA.Name = "answerA"
        Me.answerA.ReadOnly = true
        Me.answerA.Size = New System.Drawing.Size(126, 40)
        Me.answerA.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(92, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Odpowiedź A"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(278, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Odpowiedź B"
        '
        'answerB
        '
        Me.answerB.Location = New System.Drawing.Point(249, 61)
        Me.answerB.Multiline = true
        Me.answerB.Name = "answerB"
        Me.answerB.ReadOnly = true
        Me.answerB.Size = New System.Drawing.Size(126, 40)
        Me.answerB.TabIndex = 5
        '
        'Day9
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 184)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.answerB)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.answerA)
        Me.Controls.Add(Me.goBack)
        Me.Name = "Day9"
        Me.Text = "Rozwiązania - dzień 9"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents goBack As System.Windows.Forms.Button
    Friend WithEvents answerA As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents answerB As System.Windows.Forms.TextBox
End Class
