﻿Public Class Day5

    'Declare variables
    Dim functions As New Functions
    Dim data As New List(Of String)
    Dim moves() As String
    Dim moveCount As New List(Of Integer)
    Dim moveFrom As New List(Of Integer)
    Dim moveTo As New List(Of Integer)
    Dim cratesList As New List(Of String)
    Dim stacks As New List(Of List(Of String))
    Dim stack() As String
    Dim stacksArray() As String
    Dim solutionA As String
    Dim solutionB As String

    Private Sub Day5_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        functions.LoadData(Me.Name, data)

        'Prepering the data
        For index As Integer = 10 To data.Count - 1
            data(index) = data(index).Replace("move", "")
            data(index) = data(index).Replace(" from ", ",")
            data(index) = data(index).Replace(" to ", ",")
            moves = data(index).Split(",")
            moveCount.Add(moves(0))
            moveFrom.Add(moves(1))
            moveTo.Add(moves(2))
        Next

        'PART A
        functions.generateStacks("F,C,P,G,Q,R-W,T,C,P-B,H,P,M,C-L,T,Q,S,M,P,R-P,H,J,Z,V,G,N-D,P,J-L,G,P,Z,F,J,T,R-N,L,H,C,F,P,T,J-G,V,Z,Q,H,T,C,W", stacksArray, stack, cratesList, stacks)
        For index As Integer = 0 To moveCount.Count - 1
            functions.MoveItem(stacks, moveCount(index), moveFrom(index) - 1, moveTo(index) - 1, True)
        Next

        For Each Stack As List(Of String) In stacks
            solutionA = solutionA & Stack(Stack.Count - 1)
        Next

        'PART B
        stacks.Clear()
        functions.generateStacks("F,C,P,G,Q,R-W,T,C,P-B,H,P,M,C-L,T,Q,S,M,P,R-P,H,J,Z,V,G,N-D,P,J-L,G,P,Z,F,J,T,R-N,L,H,C,F,P,T,J-G,V,Z,Q,H,T,C,W", stacksArray, stack, cratesList, stacks)
        For index As Integer = 0 To moveCount.Count - 1
            functions.MoveItem(stacks, moveCount(index), moveFrom(index) - 1, moveTo(index) - 1, False)
        Next

        For Each Stack As List(Of String) In stacks
            solutionB = solutionB & Stack(Stack.Count - 1)
        Next

        answerA.Text = solutionA
        answerB.Text = solutionB
    End Sub

    Private Sub goBack_Click(sender As System.Object, e As System.EventArgs) Handles goBack.Click
        Me.Hide()
        MainForm.Show()
    End Sub
End Class