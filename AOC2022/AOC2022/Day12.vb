﻿Public Class Day12

    'Declare variables
    Dim functions As New Functions
    Dim data As New List(Of String)

    Dim solutionA As String
    Dim solutionB As String

    Private Sub Day12_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        functions.LoadData(Me.Name, data)

        answerA.Text = solutionA
        answerB.Text = solutionB
    End Sub

    Private Sub goBack_Click(sender As System.Object, e As System.EventArgs) Handles goBack.Click
        Me.Hide()
        MainForm.Show()
    End Sub
End Class