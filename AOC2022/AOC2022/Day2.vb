﻿Imports System.IO

Public Class Day2

    'Declare variables
    Dim functions As New Functions
    Dim data As New List(Of String)
    Dim enemyMove As String
    Dim myMove As String
    Dim solutionA As Integer = 0
    Dim solutionB As Integer = 0

    Private Sub Day2_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        functions.LoadData(Me.Name, data)

        For Each game As String In data
            enemyMove = game.Substring(0, 1)
            myMove = game.Substring(2, 1)

            If (enemyMove = "A") Then
                Select Case myMove
                    Case "X"
                        solutionA += 4
                        solutionB += 3
                    Case "Y"
                        solutionA += 8
                        solutionB += 4
                    Case "Z"
                        solutionA += 3
                        solutionB += 8
                End Select

            ElseIf (enemyMove = "B") Then
                Select Case myMove
                    Case "X"
                        solutionA += 1
                        solutionB += 1
                    Case "Y"
                        solutionA += 5
                        solutionB += 5
                    Case "Z"
                        solutionA += 9
                        solutionB += 9
                End Select

            ElseIf (enemyMove = "C") Then
                Select Case myMove
                    Case "X"
                        solutionA += 7
                        solutionB += 2
                    Case "Y"
                        solutionA += 2
                        solutionB += 6
                    Case "Z"
                        solutionA += 6
                        solutionB += 7
                End Select
            End If
        Next

        answerA.Text = solutionA
        answerB.Text = solutionB
    End Sub

    Private Sub goBack_Click(sender As System.Object, e As System.EventArgs) Handles goBack.Click
        Me.Hide()
        MainForm.Show()
    End Sub
End Class