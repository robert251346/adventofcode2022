﻿Imports System.IO

Public Class Functions
    'Load data from .txt fike'
    Public Sub LoadData(ByRef fileName As String, ByRef data As List(Of String))

        Dim FilePath = "D:\PROJEKTY\adventofcode2022\InputData\" & fileName & ".txt"

        If System.IO.File.Exists(FilePath) Then
            Using Reader As New StreamReader("D:\PROJEKTY\adventofcode2022\InputData\" & fileName & ".txt")
                While Reader.EndOfStream = False
                    data.Add(Reader.ReadLine())
                End While
            End Using
        Else
            MessageBox.Show("There is missing input data file!", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    'Create sublist from list of string
    Public Function CreateSublistsFromListOfString(ByRef listCount As Integer, ByRef originalList As List(Of String))
        Dim listCollection As New List(Of List(Of String))
        Dim list As List(Of String) = Nothing
        Dim size As Integer = Math.Truncate(originalList.Count / listCount)

        For i As Integer = 1 To listCount - 1
            list = New List(Of String)
            list.AddRange(originalList.GetRange(size * (i - 1), size))
            listCollection.Add(list)
        Next

        list = New List(Of String)
        list.AddRange(originalList.GetRange(size * (listCount - 1), originalList.Count - size * (listCount - 1)))
        listCollection.Add(list)

        Return listCollection
    End Function

    'Create sublist from list of integer
    Public Function CreateSublistsFromListOfInteger(ByRef listCount As Integer, ByRef originalList As List(Of Integer))
        Dim listCollection As New List(Of List(Of Integer))
        Dim list As List(Of Integer) = Nothing
        Dim size As Integer = Math.Truncate(originalList.Count / listCount)

        For i As Integer = 1 To listCount - 1
            list = New List(Of Integer)
            list.AddRange(originalList.GetRange(size * (i - 1), size))
            listCollection.Add(list)
        Next

        list = New List(Of Integer)
        list.AddRange(originalList.GetRange(size * (listCount - 1), originalList.Count - size * (listCount - 1)))
        listCollection.Add(list)

        Return listCollection

    End Function

    'Task 5 - move item function
    Public Sub MoveItem(ByRef list As List(Of List(Of String)), ByRef count As Integer, ByRef moveFrom As Integer, ByRef moveTo As Integer, ByRef reverse As Boolean)
        Dim movesArray = list(moveFrom).GetRange(list(moveFrom).Count - count, count)
        If reverse = True Then
            movesArray.Reverse()
        End If
        For Each move As String In movesArray

            list(moveFrom).RemoveAt(list(moveFrom).Count - 1)
            list(moveTo).Add(move)
        Next
    End Sub

    'Tast 5 - generate stacks
    Public Sub generateStacks(ByRef stacksToGenerate As String, ByRef stacksArray() As String, stack() As String, ByRef cratesList As List(Of String), ByRef stacks As List(Of List(Of String)))
        stacksArray = stacksToGenerate.Split("-")
        For Each eachStack As String In stacksArray
            stack = eachStack.Split(",")
            For index As Integer = 0 To stack.Count - 1
                cratesList.Add(stack(index))
            Next
            stacks.Add(cratesList)
            cratesList = New List(Of String)
        Next
    End Sub
End Class
