﻿Public Class Day4

    'Declare variables
    Dim functions As New Functions
    Dim data As New List(Of String)
    Dim pairs() As String
    Dim sections() As String
    Dim allFirstSections As New List(Of Integer)
    Dim allSecondSections As New List(Of Integer)
    Dim solutionA As Integer = 0
    Dim solutionB As Integer = 0

    Private Sub Day4_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        functions.LoadData(Me.Name, data)
        'PART A
        For Each item As String In data
            pairs = item.Split(",")
            For Each Pair As String In pairs
                sections = Pair.Split("-")
                allFirstSections.Add(sections(0))
                allSecondSections.Add(sections(1))
            Next
        Next

        Dim specificFirstSections = functions.CreateSublistsFromListOfInteger(Val(allFirstSections.Count / 2), allFirstSections)
        Dim specificSecondSections = functions.CreateSublistsFromListOfInteger(Val(allSecondSections.Count / 2), allSecondSections)

        For index As Integer = 0 To specificFirstSections.Count - 1

            Dim firstPairElement1 = specificFirstSections(index)(0)
            Dim firstPairElement2 = specificSecondSections(index)(0)
            Dim firstPairRangeEnd = firstPairElement2 - firstPairElement1

            Dim secondPairElement1 = specificFirstSections(index)(1)
            Dim secondPairElement2 = specificSecondSections(index)(1)
            Dim secondPairRangeEnd = secondPairElement2 - secondPairElement1

            If (firstPairElement1 <= secondPairElement1 And firstPairElement2 >= secondPairElement2) Then
                solutionA += 1
            ElseIf (secondPairElement1 <= firstPairElement1 And secondPairElement2 >= firstPairElement2) Then
                solutionA += 1
            End If

            'PART B
            If (firstPairElement1 >= secondPairElement1 And firstPairElement1 <= secondPairElement2) Then
                solutionB += 1
            ElseIf ((firstPairElement2 >= secondPairElement1 And firstPairElement2 <= secondPairElement2)) Then
                solutionB += 1
            ElseIf ((secondPairElement1 >= firstPairElement1 And secondPairElement1 <= firstPairElement2)) Then
                solutionB += 1
            ElseIf ((secondPairElement2 >= firstPairElement1 And secondPairElement2 <= firstPairElement2)) Then
                solutionB += 1
            End If
        Next
        answerA.Text = solutionA
        answerB.Text = solutionB
    End Sub

    Private Sub goBack_Click(sender As System.Object, e As System.EventArgs) Handles goBack.Click
        Me.Hide()
        MainForm.Show()
    End Sub
End Class