﻿Public Class Day1
    'Declare variables

    Dim functions As New Functions
    Dim data As New List(Of String)
    Dim currentCalories As Integer = 0
    Dim eachElvesCalories As New List(Of Integer)
    Dim solutionA As Integer = 0
    Dim solutionB As Integer = 0

    Private Sub Day1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        functions.LoadData(Me.Name, data)

        For Each item As String In data
            If (Not String.IsNullOrEmpty(item)) Then
                currentCalories += Val(item)
            Else
                eachElvesCalories.Add(currentCalories)
                If (currentCalories > solutionA) Then
                    solutionA = currentCalories
                    currentCalories = 0
                Else
                    currentCalories = 0
                End If
            End If
        Next

        eachElvesCalories.Sort()
        eachElvesCalories.Reverse()
        solutionB = eachElvesCalories(0) + eachElvesCalories(1) + eachElvesCalories(2)
        answerA.Text = solutionA
        answerB.Text = solutionB
    End Sub

    Private Sub goBack_Click(sender As System.Object, e As System.EventArgs) Handles goBack.Click
        Me.Hide()
        MainForm.Show()
    End Sub
End Class